Danil Kutkevich CV
==================

My curriculum vitae available in english <http://danil.kutkevich.org>
and in russian (русский язык) <http://danil.kutkevich.org/ru>.

Copyright (C) 2016 Danil Kutkevich <danil@kutkevich.org>

This CV is licensed under the Creative Commons Attribution-Share
Alike 4.0 Unported License. To view a copy of this license, see the
COPYING file or visit <http://creativecommons.org/licenses/by-sa/4.0/legalcode>
or send a letter to Creative Commons, 171 Second Street, Suite 300,
San Francisco, California, 94105, USA.
